# ncstore-back

The backend repository for ncstore project.

## Built With
- Java 8
- Maven 3
- Spring Boot
- Spring Data JPA
- Lombok

## Developers

Michael Linker - [work.michaellinker@gmail.com](work.michaellinker@gmail.com)  
Artem Bakin - [dr.artem01@yandex.ru](dr.artem01@yandex.ru)